<?php

/**
 * @file
 * Provides a library registry overview page.
 */

/**
 * Implements hook_menu().
 */
function library_registry_overview_menu() {
  $items['libraries'] = array(
    'title' => 'Libraries',
    'description' => 'A central registry of libraries.',
    'page callback' => 'library_registry_overview_table',
    'access arguments' => array('access content'),
    'file' => 'library_registry_overview.pages.inc',
  );
  return $items;
}

/**
 * Implements hook_menu_alter().
 *
 * Makes 'node/add/library' a local action on the library registry overview.
 *
 * @see library_registry_overview_form_library_node_form_alter()
 */
function library_registry_overview_menu_alter(&$items) {
  $items['libraries/add'] = $items['node/add/library'];
  $items['libraries/add']['title'] = 'Add library';
  $items['libraries/add']['type'] = MENU_LOCAL_ACTION;
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @see library_registry_overview_menu_alter()
 * @see library_registry_overview_node_form_submit()
 */
function library_registry_overview_form_library_node_form_alter(&$form, &$form_state) {
  $form['actions']['submit']['#submit'][] = 'library_registry_overview_node_form_submit';
}

/**
 * Form submission callback for the node form.
 *
 * @see library_registry_overview_form_library_node_form_alter()
 */
function library_registry_overview_node_form_submit($form, &$form_state) {
  $form_state['redirect'] = 'libraries';
}

/**
 * Loads the node associated with a library.
 *
 * Because we want to show both information from the library itself (i.e.
 * libraries_info()) as well from the associated node on the library overview
 * page, we utilize this helper function.
 *
 * @param $name
 *   The machine name of the library
 *
 * @return
 *   A node object.
 *
 * @see library_registry_overview_table()
 */
function library_registry_node($name) {
  $filename = "$name.libraries.info";
  $query = db_select('file_usage', 'fu')
    ->fields('fu', array('id'))
    ->condition('type', 'node');
  $query->join('file_managed', 'fm', 'fu.fid = fm.fid AND fm.filename = :filename', array(':filename' => $filename));
  $nid = $query->execute()->fetch()->id;
  return node_load($nid);
}