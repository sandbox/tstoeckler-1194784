<?php

/**
 * @file
 * User-facing page callbacks for library_registry_overview.module.
 */

/**
 * Shows a table of all registered libraries.
 */
function library_registry_overview_table() {
  $header = array(
    t('Name'),
    t('Machine name'),
    t('Latest version'),
    t('Info file'),
    array('data' => t('Operations'), 'colspan' => 2),
  );

  $libraries = libraries_info();
  // Sort libraries by machine name.
  ksort($libraries);

  $rows = array();
  foreach($libraries as $name => $library) {
    $node = library_registry_node($name);

    $rows[$name] = array(
      l($library['name'], $library['vendor url']),
      '<strong>' . $library['machine name'] . '</strong>',
      $node->latest_version['und'][0]['value'],
      l(t('Download'), $library['info file']),
      l('Edit', 'node/' . $node->nid . '/edit'),
      l('Delete', 'node/' . $node->nid . '/delete'),
    );
  }

  return theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'empty' => t('No libraries have been registered.'),
  ));
}
