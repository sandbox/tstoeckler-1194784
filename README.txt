Library registry
----------------

INSTALLATION
------------
Install as you would any other installation profile. The library registry aims
to enable users to download libaries' info files. The default .htaccess file
that ships with Drupal core prevents the downloading of .info files, though.
For this purpose you need to explicitly allow the downloading of .libraries.info
files in the sites/default/files/libraries directory (if sites/default/files is
the location of your public files).
For people running an Apache webserver the file "example.htaccess" is provided
to do just that. Rename it to ".htaccess" and place it in the in the above
mentioned directory. Afterwards, restart Apache. Do not place this file in the
Drupal root directory or anywhere other than the directory mentioned above! That
might make downloading the library files work, but is a critical security
vulnerability!
