<?php

/**
 * @file
 * Central library registry for Drupal.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function library_registry_form_install_configure_form_alter(&$form, &$form_state) {
  $form['site_information']['site_name']['#default_value'] = 'Library registry';
}
